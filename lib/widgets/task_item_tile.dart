import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';

class TaskItemTile extends StatefulWidget {
    
    final Task _task;
    
    TaskItemTile(this._task);

    @override
    TaskItemTileState createState() => TaskItemTileState();
}

class TaskItemTileState extends State<TaskItemTile> {    
    late bool _isDone;
    
    @override
    void initState(){
        super.initState();
        setState((){
            _isDone = widget._task.isDone == 1; //for sqlLite no boolean value, kaya 1 ang gamit
        });
    }
    
    @override
    Widget build(BuildContext context) {
        Task task = widget._task;
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        return CheckboxListTile(
            title: Text(task.description),
            value: _isDone,
            controlAffinity: ListTileControlAffinity.leading,
            secondary: IconButton(
                icon: Icon(Icons.info),
                onPressed: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) {
                                return TaskDetailScreen(task);
                            }
                        )
                    );
                }
            ),
            onChanged: (bool? value) {
                API(accessToken ).toggleTaskStatus(
                    id: task.id, 
                    isDone: (_isDone == true) ? 1 : 0
                ).then((value){
                    if(value == true){
                        setState(() => _isDone = !_isDone);
                    }
                }).catchError((error){
                    showSnackBar(context, error.message);
                });
                //direct execution, since mag aupdate lang 
            }
        );
    }
}