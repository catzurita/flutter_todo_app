import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/providers/user_provider.dart';

class DrawerList extends StatelessWidget {
 
    @override
    Widget build(BuildContext context) {
        return ListView(
            children: [
                ListTile(
                    title: Text('Logout'),
                    hoverColor: Color.fromRGBO(255, 212, 71, 1),
                    onTap: () async {
                        Provider.of<UserProvider>(context, listen:false).setAccessToken(null);
                        Provider.of<UserProvider>(context, listen:false).setUserId(null);

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        
                        prefs.remove('accessToken');
                        prefs.remove('userId');

                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                    }
                )
            ],
        );
    }
}