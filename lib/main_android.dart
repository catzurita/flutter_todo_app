import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/screens/login_screen.dart';
import '/screens/register_screen.dart';
import '/screens/task_list_screen.dart';

import '/providers/user_provider.dart';

Future<void> main() async{
    // Initial checks for user's access token from SharedPreferences
    // Determine initiail route of app depending on existence of user's access token

    WidgetsFlutterBinding.ensureInitialized();
    await dotenv.load(fileName: "assets/env/.env_android");

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken'); //getting the value of the accessToken in type String
    String initialRoute = (accessToken != null) ? '/task-list' : '/';  // will assign the initial route depending if there is a user logged in or not 
    
    runApp(App(accessToken, initialRoute));
}

class App extends StatelessWidget{
    final String? _accessToken;
    final String _initialRoute;
    
    App(this._accessToken, this._initialRoute);

    @override
    Widget build(BuildContext context){
        return ChangeNotifierProvider(
            create: (BuildContext context) => UserProvider(_accessToken),
            child: MaterialApp( 
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(255, 212, 71, 1),
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(255, 212, 71, 1),
                            onPrimary: Colors.black
                        )
                    )
                ),
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/register': (context) => RegisterScreen(),
                    '/task-list': (context) => TaskListScreen()
                },
            )
        
        );
    }
}

/* 
    Using the sharedPreferences 

    this type of code will allow us to know if there is already someone who is logged in at the start of the app
 */