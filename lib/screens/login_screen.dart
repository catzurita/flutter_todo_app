import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/user.dart';
import '/providers/user_provider.dart';

class LoginScreen extends StatefulWidget{

    @override
    LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>{
    
    Future<User>? _futureLogin;
    
    final _formKey = GlobalKey<FormState>();
    final _emailController = TextEditingController();
    final _passwordController = TextEditingController();

    void login(BuildContext context){
        setState(() {
            _futureLogin = API().login(
              email: _emailController.text, 
              password: _passwordController.text
            ).catchError((error){ //yung ithrothrow na error from the api, dito macacatch as the error message
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context){
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setUserId = Provider.of<UserProvider>(context, listen: false).setUserId;
        
        Widget email = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _emailController,
            validator: (email){
                if(email == null || email.isEmpty){
                    return 'The email must be provided';
                } else if (EmailValidator.validate(email) == false){
                    return 'A valid email must be provided';
                } else { 
                    return null;
                }
            },
        );
        
        Widget password = TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller: _passwordController,
            validator: (password){
                bool isValid = password != null && password.isNotEmpty;
                return isValid ? null : 'The password must be provided';
            },
        );

        Widget submit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Login'),
                onPressed: (){
                    if(_formKey.currentState!.validate()){
                        login(context);      
                    } else {
                        print('The login form is not valid');
                    }
                }
            )
        );
        
        Widget goToRegister = Container(
            width: double.infinity,
            child: ElevatedButton(
                child: Text('No Account Yet?'),
                onPressed: (){
                    Navigator.pushNamed(context, '/register');
                }
            )
        );
        
        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    email,
                    password,
                    submit,
                    goToRegister
                ],
            ),
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder:(context, snapshot){
                if(_futureLogin == null){
                    return formLogin;
                } else if (snapshot.hasError == true){
                    print(snapshot);
                    return formLogin;
                } else if (snapshot.hasData == true) {
                        Timer(Duration(milliseconds: 1), () async {
                        // if sa instance na wala pang user na nakalogged in, pag ka logged in ng new user, isave sa prefs yung file para sa next gamit, makikita na yung value ni user na nakalogged in na 
                        SharedPreferences prefs = await SharedPreferences.getInstance(); 
                        User user = snapshot.data as User;

                        setUserId(user.id);
                        setAccessToken(user.accessToken);

                        prefs.setString('accessToken', user.accessToken!);
                        prefs.setInt('userId', user.id);

                        Navigator.pushReplacementNamed(context, '/task-list');
                    });
                    return Container();
                }
                return Center(
                    child: CircularProgressIndicator(),
                );
            }
        );
        
        return Scaffold(
            appBar: AppBar(
                title: Text('Todo Login'),
            ),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: loginView,
            )
        );
    }
}