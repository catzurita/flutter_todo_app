import 'dart:async';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';

import '/utils/api.dart';
import '/utils/functions.dart';

class RegisterScreen extends StatefulWidget {
    @override
    RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
     
    Future<bool>? _futureUser;

    final _formKey = GlobalKey<FormState>();
    final _emailController = TextEditingController();
    final _passwordController = TextEditingController();
    final _confirmPassword = TextEditingController();

    void registerUser(BuildContext context){ //ginamit lang yung context para sa snackbar
        setState(() {
            _futureUser = API().register(
              email: _emailController.text, 
              password: _passwordController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget email = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _emailController,
            validator: (email){
                if(email == null || email.isEmpty){
                    return 'The email must be provided';
                } else if (EmailValidator.validate(email) == false){
                    return 'A valid email must be provided';
                } else { 
                    return null;
                }
            },
        );

        Widget password = TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller: _passwordController,
            validator: (password){
                bool isValid = password != null && password.isNotEmpty;
                return isValid ? null : 'The password must be provided';
            },
        );

        Widget confirmPassword = TextFormField(
            decoration: InputDecoration(labelText: 'Confirm Password'),
            obscureText: true,
            controller: _confirmPassword,
            validator: (passwordConfirmation){
                if(passwordConfirmation == null || passwordConfirmation.isEmpty){
                    return 'The password confirmation must be provided';
                } else if(_passwordController.text != passwordConfirmation){
                    return 'The password confirmation does not match with the password above';
                } else {
                    return null;
                }
            },
        );

        Widget register = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Register'),
                onPressed: (){
                    if(_formKey.currentState!.validate()){
                        registerUser(context);
                    } else {
                        showSnackBar(context, 'Update registration fields to pass all validations.');
                    }
                }
            )
        );

        Widget goToLogin = Container(
            width: double.infinity,
            child: ElevatedButton(
                child: Text('Go to Login'),
                onPressed: () {
                    Navigator.pop(context);
                }
            )
        );

        Widget formRegister = SingleChildScrollView(
                child: Form(
                key: _formKey, 
                child: Column(
                    children: [
                        email,
                        password,
                        confirmPassword,
                        register,
                        goToLogin
                    ]
                )
            )
        );
        
        Widget registerView = FutureBuilder(
            future: _futureUser, //mag uupdate si FutureBuilder if matrigger si _futureUser
            builder: (context, snapshot){ // snapshot is the response
                print(snapshot.hasData);
                print(snapshot.hasError);
                if(_futureUser == null){
                    return formRegister;
                } else if (snapshot.hasData && snapshot.data == true) {
                    Timer( Duration(seconds: 3), (){
                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                    });
                    return Column(
                        children: [
                            Text('Registration successful. You will be redirected shortly back to the login page.'),
                        ],
                    );
                } else {
                    return Center(
                        child: CircularProgressIndicator(),
                    ); 
                }
            }
        );

        return Scaffold(
            appBar: AppBar(
                title: Text('Todo Account Registration'),
            ),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: registerView
            )
        );
    }
}