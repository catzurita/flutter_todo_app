import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/task.dart';
import '/providers/user_provider.dart';

class TaskDetailScreen extends StatefulWidget {
    final Task _task;

    TaskDetailScreen(this._task);
    @override
    TaskDetailScreenState createState() => TaskDetailScreenState();
}

class TaskDetailScreenState extends State<TaskDetailScreen> {
    Future<Task>? _futureImageUpload;

    @override
    Widget build(BuildContext context) {
        Task task = widget._task;
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget upload = Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
                child: Text('Upload Image'),
                onPressed: () async {
                    var selectedImage = await ImagePicker().pickImage(
                        source: ImageSource.gallery
                    );
                    setState((){
                        if (selectedImage != null){
                            _futureImageUpload = API(accessToken).addTaskImage(
                                id: task.id,
                                filePath: selectedImage.path
                            ).catchError((error){
                                showSnackBar(context, error.message);
                            });
                        }
                    });
                }
            )
        );

        Widget imageView = FutureBuilder(
            future: _futureImageUpload,
            builder: (context, snapshot) {
                return Container();
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Detail')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Text(task.description),
                            upload,
                            imageView
                        ]
                    )
                )
            )
        );
    }
}