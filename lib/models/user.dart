import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User{
    const factory User({
        required int id,
        required String email,
        String? accessToken
    }) = _User;

    // This line is a part of the implementation of the package json serializable 
    factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

// class User {
//     late final int? id;
//     late final String? email;
//     late final String? accessToken;

//     User({ 
//         this.id, 
//         this.email,
//         this.accessToken
//     });

//     factory User.fromJson(Map<String, dynamic> json) {
//         return User(
//             id: json['id'],
//             email: json['email'],
//             accessToken: json['accessToken']
//         );
//     }
// }

//Model-View-Controller
//View (flutter) <=> Model(class)
//Controller (API) <=> Model (database)